package fr.sedoo.commons.metadata.utils;


/**
 * 
 * @author Francois ANDRE
 * 
 */
public class TestMetadataWithAcquisition

{
//	static String resourceTitle = "resourceTitle";
//	static String resourceAbstract = "resourceAbstract";
//	static String resourceDOI = "10.1007/s00223-003-0070-0";
//	static List<String> resourceLanguageCodes = new ArrayList()
//	{
//		{
//			add("fre");
//			add("eng");
//		}
//	};
//	static List<String> keywords = new ArrayList()
//	{
//		{
//			add("traitement de l'eau");
//			add("bassin versant");
//		}
//	};
//	static List<DescribedURL> resourceURL = new ArrayList<DescribedURL>()
//	{
//		{
//			add(new DescribedURL("http://www.google.com", "google"));
//			add(new DescribedURL("http://www.lemonde.fr", "lemonde"));
//		}
//	};
//	static List<DescribedURL> snapshotURL = new ArrayList<DescribedURL>()
//	{
//		{
//			add(new DescribedURL("http://i.dailymail.co.uk/i/pix/2012/11/15/article-0-1609D0FF000005DC-373_964x641.jpg", "desert"));
//			add(new DescribedURL("http://www.tela-botanica.org/actu/IMG/appli_cel-img_000093340M.jpg", "fleur"));
//		}
//	};
//	static String metadataLanguageCode = "eng";
//	static String metadataDate = "2013-10-26";
//	static String publicationDate = "2012-09-25";
//	static String creationDate = "2011-08-24";
//	static String lastRevisionDate = "2010-07-23";
//	static String uuid = "550e8400-e29b-41d4-a716-446655440000";
//	// static double northBoundLatitude = 3.17;
//	// static double southBoundLatitude = 41.56;
//	// static double eastBoundLongitude = 8.49;
//	// static double westBoundLongitude = 11.83;
//	// static double eastBoundLongitude = 11.83;
//	// static double southBoundLatitude = 3.17;
//
//	static double northBoundLatitude = 67.9476;
//	static double southBoundLatitude = 61.3623;
//	static double westBoundLongitude = -28.125;
//	static double eastBoundLongitude = -7.7344;
//
//	static String startDate = "1995-01-22";
//	static DefaultGeographicBoundingBox boundingbox = new DefaultGeographicBoundingBox(westBoundLongitude, eastBoundLongitude, southBoundLatitude, northBoundLatitude);
//	static String metadataUseConditions = "useContions";
//	private static String metadataPublicAccessLimitations = "accessLimitations";
//	private static String observatoryName = "My Observatory name";
//	private static String basinName = "My drainage basin name";
//
//	final Logger logger = LoggerFactory.getLogger(TestMetadataWithAcquisition.class);
//
//	public static RBVMetadata getTestMetadata() throws IllegalStateException, URISyntaxException
//	{
//		RBVMetadata metadata = new RBVMetadata();
//		metadata.setResourceTitle(resourceTitle);
//		metadata.setResourceAbstract(resourceAbstract);
//		metadata.setResourceLanguages(resourceLanguageCodes);
//		metadata.setMetadataLanguage(metadataLanguageCode);
//		metadata.setKeywords(keywords);
//		metadata.setResourceURL(resourceURL);
//		metadata.setSnapshotURL(snapshotURL);
//		metadata.setPublicationDate(publicationDate);
//		metadata.setCreationDate(creationDate);
//		metadata.setLastRevisionDate(lastRevisionDate);
//		metadata.setMetadataDate(metadataDate);
//		metadata.setGeographicBoundingBox(boundingbox);
//		metadata.setUseConditions(metadataUseConditions);
//		metadata.setPublicAccessLimitations(metadataPublicAccessLimitations);
//		metadata.setResourceBeginDate("2001-12-25");
//		// metadata.setResourceEndDate("2008-12-25");
//		metadata.setResourceEndDate(TimeIndeterminateValueType.NOW.name());
//		metadata.setResourceContacts(getResourceContacts());
//		metadata.setMetadataContacts(getMetadataContacts());
//		metadata.setResourceEncodingCharset("UTF8");
//		DefaultFormat format = new DefaultFormat();
//		format.setName(new DefaultInternationalString("Excel"));
//		format.setVersion(new DefaultInternationalString("97"));
//		metadata.setResourceFormat(format);
//		metadata.setUuid(uuid);
//		metadata.setStatus(StatusLabelProvider.HISTORICAL_ARCHIVE);
//		metadata.setResourceUpdateRythm(UpdateRythmLabelProvider.AS_NEEDED);
//		metadata.setObservatoryName(observatoryName);
//		metadata.setDrainageBasinName(basinName);
//		metadata.setObservatoryDescription("ligne1\\nLigne2\\nLigne3");
//
//		DefaultAcquisitionInformation acquisitionInformation = new DefaultAcquisitionInformation();
//		DefaultInstrument instrument = new DefaultInstrument();
//		String sensorMLUrl = "https://marinemetadata.org/files/mmi/examples/mmihostedwork/ontologieswork/mmiworkshop06/materials/track1/sensorml/EXAMPLES/Garmin_GPS_17HVS.xml";
//
//		XLink link = new XLink();
//		link.setType(Type.SIMPLE);
//		link.setHRef(new URI(sensorMLUrl));
//
//		instrument.getIdentifierMap().putSpecialized(IdentifierSpace.XLINK, link);
//
//		instrument.setDescription(new DefaultInternationalString("Instrument de mesure de la marine américaine"));
//		acquisitionInformation.setInstruments(Collections.singletonList(instrument));
//		metadata.setAcquisitionInformation(Collections.singletonList(acquisitionInformation));
//		metadata.setUuid(UUID.randomUUID().toString());
//		DefaultLineage lineage = new DefaultLineage();
//
//		return metadata;
//	}
//
//	private static List<Contact> getResourceContacts()
//	{
//		List<Contact> contacts = new ArrayList<Contact>();
//		contacts.add(getContact(1, RoleLabelProvider.AUTHOR));
//		contacts.add(getContact(2, RoleLabelProvider.OWNER));
//		contacts.add(getContact(3, RoleLabelProvider.RESOURCE_PROVIDER));
//		return contacts;
//	}
//
//	private static List<Contact> getMetadataContacts()
//	{
//		List<Contact> contacts = new ArrayList<Contact>();
//		contacts.add(getContact(11, RoleLabelProvider.POINT_OF_CONTACT));
//		contacts.add(getContact(21, RoleLabelProvider.AUTHOR));
//		return contacts;
//	}
//
//	private static Contact getContact(int i, String role)
//	{
//		Contact contact = new Contact();
//		contact.setEmailAddress("ceci.est.mon.adresse_" + i + "@monsite.fr");
//		contact.setIndividualName("Nom Prénom " + i);
//		contact.setOrganisationName("Mon organisation " + i);
//		contact.setRole(role);
//		return contact;
//	}
//
//	@Test
//	public void testMarshalling() throws Exception
//	{
//		String result = MetadataTools.toISO19139(getTestMetadata());
//		logger.debug("\n" + result);
//
//		// System.out.println(period);
//
//		/*
//		 * String content = FileUtils.readFileToString(new File(
//		 * "C:/Users/francois/git/portailrbv/portailrbv-core/src/test/resources/samples/sample3.xml"
//		 * )); Metadata aux = MetadataTools.fromXML(content);
//		 * System.out.println(aux);
//		 */
//	}

}
