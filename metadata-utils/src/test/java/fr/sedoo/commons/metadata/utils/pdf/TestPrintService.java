package fr.sedoo.commons.metadata.utils.pdf;

import java.util.ArrayList;
import java.util.Locale;

import org.junit.Test;

import fr.sedoo.commons.metadata.shared.utils.Iso6392LanguageConverter;
import fr.sedoo.commons.metadata.utils.TestMetadata;
import fr.sedoo.commons.metadata.utils.domain.MetadataTools;
import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.commons.test.TestTools;

public class TestPrintService
{

	private static String OS = System.getProperty("os.name").toLowerCase();
	
	private static ArrayList<Locale> locales = new ArrayList<Locale>();
	static
	{
		locales.add(new Locale(Iso6392LanguageConverter.convertIso6392Tolocale(Iso6392LanguageConverter.ENGLISH)));
		locales.add(new Locale(Iso6392LanguageConverter.convertIso6392Tolocale(Iso6392LanguageConverter.FRENCH)));
	}
	
	private static PrintService englishService = new PrintService(new Locale(Iso6392LanguageConverter.convertIso6392Tolocale(Iso6392LanguageConverter.ENGLISH)), locales);
	private static PrintService frenchService = new PrintService(new Locale(Iso6392LanguageConverter.convertIso6392Tolocale(Iso6392LanguageConverter.FRENCH)), locales);

	@Test
	public void testCompleteMetadata() throws Exception
	{
		String frenchFileName = "/tmp/testFR.pdf";
		String englishFileName = "/tmp/testEN.pdf";
		if (isWindows())
		{
			frenchFileName = "C:/tmp/testFR.pdf";
			englishFileName = "C:/tmp/testEN.pdf";
		}

		SedooMetadata metadata = TestMetadata.getMultilingualMetadata();

		frenchService.setOutputFileName(frenchFileName);
		frenchService.metadataToPdf(metadata);

		englishService.setOutputFileName(englishFileName);
		englishService.metadataToPdf(metadata);
	}

	@Test
	public void testEmptyMetadata() throws Exception
	{
		String frenchFileName = "/tmp/testEmptyFR.pdf";
		String englishFileName = "/tmp/testEmptyEN.pdf";
		if (isWindows())
		{
			frenchFileName = "C:/tmp/testEmptyFR.pdf";
			englishFileName = "C:/tmp/testEmptyEN.pdf";
		}

		SedooMetadata metadata = new SedooMetadata();

		
		frenchService.setOutputFileName(frenchFileName);
		frenchService.metadataToPdf(metadata);

		englishService.setOutputFileName(englishFileName);
		englishService.metadataToPdf(metadata);
	}

	public static boolean isWindows()
	{

		return (OS.indexOf("win") >= 0);

	}

	@Test
	public void testAmmacatchMetadata() throws Exception
	{
		String frenchFileName = "/tmp/testAmmaCatch.pdf";
		String englishFileName = "/tmp/testAmmaCatchEN.pdf";
		if (isWindows())
		{
			frenchFileName = "C:/tmp/testAmmaCatch.pdf";
			englishFileName = "C:/tmp/testAmmaCatchEN.pdf";
		}

		String xml = TestTools.getResourceFileContent(this.getClass(), "ammacatch.txt");

		SedooMetadata metadata = MetadataTools.fromISO19139(xml);

		frenchService.setOutputFileName(frenchFileName);
		frenchService.metadataToPdf(metadata);

		englishService.setOutputFileName(englishFileName);
		englishService.metadataToPdf(metadata);
	}
	
	@Test
	public void testBvetMetadata() throws Exception
	{
		String frenchFileName = "/tmp/testBvet.pdf";
		String englishFileName = "/tmp/testBvetEn.pdf";
		if (isWindows())
		{
			frenchFileName = "C:/tmp/testBvet.pdf";
			englishFileName = "C:/tmp/testBvetEN.pdf";
		}

		String xml = TestTools.getResourceFileContent(this.getClass(), "bvet.txt");

		SedooMetadata metadata = MetadataTools.fromISO19139(xml);

		frenchService.setOutputFileName(frenchFileName);
		frenchService.metadataToPdf(metadata);

		englishService.setOutputFileName(englishFileName);
		englishService.metadataToPdf(metadata);
	}
}
