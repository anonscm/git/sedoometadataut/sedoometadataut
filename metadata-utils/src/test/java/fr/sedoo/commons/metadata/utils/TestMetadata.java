package fr.sedoo.commons.metadata.utils;

import java.util.HashMap;

import fr.sedoo.commons.metadata.shared.utils.Iso6392LanguageConverter;
import fr.sedoo.commons.metadata.utils.domain.MetadataTools;
import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;


/**
 * 
 * @author Francois ANDRE
 * 
 */
public class TestMetadata

{
//	static String resourceTitle = "resourceTitle";
//	static String resourceAbstract = "resourceAbstract";
//	static String resourceDOI = "10.1007/s00223-003-0070-0";
//	static List<String> resourceLanguageCodes = new ArrayList()
//	{
//		{
//			add("fre");
//			add("eng");
//		}
//	};
//	static List<String> keywords = new ArrayList()
//	{
//		{
//			add("traitement de l'eau");
//			add("bassin versant");
//		}
//	};
//	static List<DescribedURL> resourceURL = new ArrayList<DescribedURL>()
//	{
//		{
//			add(new DescribedURL("http://www.google.com", "google"));
//			add(new DescribedURL("http://www.lemonde.fr", "lemonde"));
//		}
//	};
//	static List<DescribedURL> snapshotURL = new ArrayList<DescribedURL>()
//	{
//		{
//			add(new DescribedURL("http://i.dailymail.co.uk/i/pix/2012/11/15/article-0-1609D0FF000005DC-373_964x641.jpg", "desert"));
//			add(new DescribedURL("http://www.tela-botanica.org/actu/IMG/appli_cel-img_000093340M.jpg", "fleur"));
//		}
//	};
//	static String metadataLanguageCode = "eng";
//	static String metadataDate = "2013-10-26";
//	static String publicationDate = "2012-09-25";
//	static String creationDate = "2011-08-24";
//	static String lastRevisionDate = "2010-07-23";
//	static String uuid = "550e8400-e29b-41d4-a716-446655440000";
//	// static double northBoundLatitude = 3.17;
//	// static double southBoundLatitude = 41.56;
//	// static double eastBoundLongitude = 8.49;
//	// static double westBoundLongitude = 11.83;
//	// static double eastBoundLongitude = 11.83;
//	// static double southBoundLatitude = 3.17;
//
//	static double northBoundLatitude = 67.9476;
//	static double southBoundLatitude = 61.3623;
//	static double westBoundLongitude = -28.125;
//	static double eastBoundLongitude = -7.7344;
//
//	static String startDate = "1995-01-22";
//	static DefaultGeographicBoundingBox boundingbox = new DefaultGeographicBoundingBox(westBoundLongitude, eastBoundLongitude, southBoundLatitude, northBoundLatitude);
//	static String metadataUseConditions = "useContions";
//	private static String metadataPublicAccessLimitations = "accessLimitations";
//	private static String observatoryName = "My Observatory name";
//	private static String basinName = "My drainage basin name";
//
//	final Logger logger = LoggerFactory.getLogger(TestMetadata.class);
//
//	@Test
//	public void testSetters() throws Exception
//	{
//		RBVMetadata metadata = getTestMetadata();
//		Assert.assertEquals("La donnée resourceTitle doit être conservée", resourceTitle, metadata.getResourceTitle());
//		Assert.assertEquals("La donnée resourceAbstract doit être conservée", resourceAbstract, metadata.getResourceAbstract());
//		Assert.assertEquals("La donnée publicationDate doit être conservée", publicationDate, metadata.getPublicationDate());
//		Assert.assertEquals("La donnée creationDate doit être conservée", creationDate, metadata.getCreationDate());
//		Assert.assertEquals("La donnée lastRevisionDate doit être conservée", lastRevisionDate, metadata.getLastRevisionDate());
//		Assert.assertEquals("La donnée metadataDate doit être conservée", metadataDate, metadata.getMetadataDate());
//		Assert.assertEquals("La donnée boundingbox doit être conservée", boundingbox, metadata.getGeographicBoundingBox());
//		Assert.assertTrue("La donnée resourceLanguageCodes doit être conservée", areStringListEqual(resourceLanguageCodes, metadata.getResourceLanguages()));
//		Assert.assertTrue("La donnée resourceURL doit être conservée", areDescribedURLListEqual(resourceURL, metadata.getResourceURL()));
//		Assert.assertTrue("La donnée snapshotURL doit être conservée", areDescribedURLListEqual(snapshotURL, metadata.getSnapshotURL()));
//		Assert.assertEquals("La donnée metadataLanguageCode doit être conservée", metadataLanguageCode, metadata.getMetadataLanguage());
//		Assert.assertEquals("La donnée metadataUseConditions doit être conservée", metadataUseConditions, metadata.getUseConditions());
//
//		// Données initialisés
//		Assert.assertEquals("La donnée hierachyLevels doit contenir une seule entrée", 1, metadata.getHierarchyLevels().size());
//
//		// BrowseGraphic
//	}
//
//	private boolean areDescribedURLListEqual(List<DescribedURL> first, List<DescribedURL> second)
//	{
//		if ((first == null) && (second == null))
//		{
//			return true;
//		}
//		if ((first != null) && (second == null))
//		{
//			return false;
//		}
//		if ((first == null) && (second != null))
//		{
//			return false;
//		}
//		if (first.size() != second.size())
//		{
//			return false;
//		}
//
//		Iterator<DescribedURL> firstIterator = first.iterator();
//		Iterator<DescribedURL> secondIterator = second.iterator();
//
//		while (firstIterator.hasNext())
//		{
//			DescribedURL aux1 = firstIterator.next();
//			DescribedURL aux2 = secondIterator.next();
//			if (aux1.getLabel().compareTo(aux2.getLabel()) != 0)
//			{
//				return false;
//			}
//
//			if (aux1.getLink().compareTo(aux2.getLink()) != 0)
//			{
//				return false;
//			}
//		}
//
//		return true;
//	}
//
//	public static RBVMetadata getTestMetadata()
//	{
//		RBVMetadata metadata = new RBVMetadata();
//		metadata.setResourceTitle(resourceTitle);
//		metadata.setResourceAbstract(resourceAbstract);
//		metadata.setResourceLanguages(resourceLanguageCodes);
//		metadata.setMetadataLanguage(metadataLanguageCode);
//		metadata.setKeywords(keywords);
//		metadata.setResourceURL(resourceURL);
//		metadata.setSnapshotURL(snapshotURL);
//		metadata.setPublicationDate(publicationDate);
//		metadata.setCreationDate(creationDate);
//		metadata.setLastRevisionDate(lastRevisionDate);
//		metadata.setMetadataDate(metadataDate);
//		metadata.setGeographicBoundingBox(boundingbox);
//		metadata.setUseConditions(metadataUseConditions);
//		metadata.setPublicAccessLimitations(metadataPublicAccessLimitations);
//		metadata.setResourceBeginDate("2001-12-25");
//		// metadata.setResourceEndDate("2008-12-25");
//		metadata.setResourceEndDate(TimeIndeterminateValueType.NOW.name());
//		metadata.setResourceContacts(getResourceContacts());
//		metadata.setMetadataContacts(getMetadataContacts());
//		metadata.setResourceEncodingCharset("UTF8");
//		DefaultFormat format = new DefaultFormat();
//		format.setName(new DefaultInternationalString("Excel"));
//		format.setVersion(new DefaultInternationalString("97"));
//		metadata.setResourceFormat(format);
//		metadata.setUuid(uuid);
//		metadata.setStatus(StatusLabelProvider.HISTORICAL_ARCHIVE);
//		metadata.setResourceUpdateRythm(UpdateRythmLabelProvider.AS_NEEDED);
//		metadata.setObservatoryName(observatoryName);
//		metadata.setDrainageBasinName(basinName);
//		metadata.setObservatoryDescription("ligne1\\nLigne2\\nLigne3");
//		return metadata;
//	}
//
//	private static List<Contact> getResourceContacts()
//	{
//		List<Contact> contacts = new ArrayList<Contact>();
//		contacts.add(getContact(1, RoleLabelProvider.AUTHOR));
//		contacts.add(getContact(2, RoleLabelProvider.OWNER));
//		contacts.add(getContact(3, RoleLabelProvider.RESOURCE_PROVIDER));
//		return contacts;
//	}
//
//	private static List<Contact> getMetadataContacts()
//	{
//		List<Contact> contacts = new ArrayList<Contact>();
//		contacts.add(getContact(11, RoleLabelProvider.POINT_OF_CONTACT));
//		contacts.add(getContact(21, RoleLabelProvider.AUTHOR));
//		return contacts;
//	}
//
//	private static Contact getContact(int i, String role)
//	{
//		Contact contact = new Contact();
//		contact.setEmailAddress("ceci.est.mon.adresse_" + i + "@monsite.fr");
//		contact.setIndividualName("Nom Prénom " + i);
//		contact.setOrganisationName("Mon organisation " + i);
//		contact.setRole(role);
//		return contact;
//	}
//
//	@Test
//	public void testInspireValidation() throws Exception
//	{
//		RBVMetadata metadata = getTestMetadata();
//
//		String result = MetadataTools.toISO19139(metadata);
//		logger.debug("\n" + result);
//
//		// HttpPost httpPost = new
//		// HttpPost("http://www.inspire-geoportal.eu/INSPIREValidatorService/resources/validation/inspire");
//		HttpPost httpPost = new HttpPost("http://inspire-geoportal.ec.europa.eu/GeoportalProxyWebServices/resources/INSPIREResourceTester");
//		// xml response: h
//		httpPost.addHeader("Accept", "application/xml");
//		// html response
//		// httpPost.addHeader("Accept", "text/html");
//		File tempFile = File.createTempFile("fan", "fan");
//		FileUtils.writeStringToFile(tempFile, result);
//
//		// ContentBody body = new StringBody(outputStream.toString());
//		ContentBody body = new FileBody(tempFile);
//		MultipartEntity reqEntity = new MultipartEntity();
//		reqEntity.addPart("dataFile", body);
//		httpPost.setEntity(reqEntity);
//		HttpClient client = new DefaultHttpClient();
//		HttpResponse response = client.execute(httpPost);
//		String globalErrorMessage = InspireValidatorResponseParser.parseResponse(response);
//		Assert.assertEquals("L'analyse Inspire ne doit pas renvoyer d'erreurs", "", globalErrorMessage);
//		tempFile.delete();
//	}
//
//	private static boolean areStringListEqual(List<String> first, List<String> second)
//	{
//		if ((first == null) && (second == null))
//		{
//			return true;
//		}
//		if ((first != null) && (second == null))
//		{
//			return false;
//		}
//		if ((first == null) && (second != null))
//		{
//			return false;
//		}
//		if (first.size() != second.size())
//		{
//			return false;
//		}
//
//		Iterator<String> firstIterator = first.iterator();
//		Iterator<String> secondIterator = second.iterator();
//
//		while (firstIterator.hasNext())
//		{
//			String aux1 = firstIterator.next();
//			String aux2 = secondIterator.next();
//			if (aux1.compareTo(aux2) != 0)
//			{
//				return false;
//			}
//		}
//
//		return true;
//
//	}
//
//	@Test
//	public void testMarshalling() throws Exception
//	{
//		String result = MetadataTools.toISO19139(getTestMetadata());
//		logger.debug("\n" + result);
//
//		// System.out.println(period);
//
//		/*
//		 * String content = FileUtils.readFileToString(new File(
//		 * "C:/Users/francois/git/portailrbv/portailrbv-core/src/test/resources/samples/sample3.xml"
//		 * )); Metadata aux = MetadataTools.fromXML(content);
//		 * System.out.println(aux);
//		 */
//	}
//
//	@Test
//	public void testTimePeriod() throws Exception
//	{
//		DefaultTemporalExtent defExtent = new DefaultTemporalExtent();
//		List<DefaultTemporalExtent> defExtList = new ArrayList<DefaultTemporalExtent>();
//
//		DefaultPeriod timePeriod = new DefaultPeriod();
//		timePeriod.setBegining(Calendar.getInstance().getTime());
//		timePeriod.setEnding(Calendar.getInstance().getTime());
//
//		defExtent.setExtent(timePeriod); // set TemporalExtent
//		defExtList.add(defExtent);
//
//		System.out.println(MetadataTools.toISO19139(defExtent));
//
//		TimePositionType beginPosition = new TimePositionType(Calendar.getInstance().getTime());
//
//		TimePeriodType period = new TimePeriodType(beginPosition);
//
//		period.setEndPosition(TimeIndeterminateValueType.NOW);
//
//		/*
//		 * TimePositionType position = new TimePositionType();
//		 * position.setIndeterminatePosition(TimeIndeterminateValueType.NOW);
//		 * TimePeriodType period = new TimePeriodType(position);
//		 */
//		defExtent.setExtent(period); // set TemporalExtent
//
//		System.out.println(MetadataTools.toISO19139(defExtent));
//
//	}
//
//	@Test
//	public void testIndeterminateTimePeriod() throws Exception
//	{
//		DefaultTemporalExtent defExtent = new DefaultTemporalExtent();
//		TimePositionType beginPosition = new TimePositionType(Calendar.getInstance().getTime());
//		TimePeriodType period = new TimePeriodType(beginPosition);
//		period.setEndPosition(TimeIndeterminateValueType.NOW);
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//		defExtent.setExtent(period);
//		XML.marshal(defExtent, outputStream);
//		String result = outputStream.toString();
//		System.out.println(result);
//	}
//
//	@Test
//	public void testEmptyMetadata() throws Exception
//	{
//		RBVMetadata metadata = new RBVMetadata();
//		metadata.setResourceBeginDate("");
//		Assert.assertEquals("", metadata.getResourceBeginDate());
//		metadata.setResourceEndDate("");
//		Assert.assertEquals("", metadata.getResourceEndDate());
//	}
//
//	@Test
//	public void testModificationResourceBeginDate() throws Exception
//	{
//		RBVMetadata metadata = new RBVMetadata();
//		metadata.setResourceBeginDate("2000-01-01");
//		Assert.assertEquals("2000-01-01", metadata.getResourceBeginDate());
//		metadata.setResourceBeginDate("2002-01-01");
//		metadata.setResourceEndDate("2022-01-01");
//		Assert.assertEquals("2002-01-01", metadata.getResourceBeginDate());
//		Assert.assertEquals("2022-01-01", metadata.getResourceEndDate());
//
//	}
//
//	@Test
//	public void testRBVMetadataExtent() throws Exception
//	{
//		RBVMetadataExtent extent = new RBVMetadataExtent();
//		extent.getLocalisationExtent().setDrainageBasinName("My drainageBasin");
//		extent.getLocalisationExtent().setObservatoryName("My observatory");
//		extent.getLocalisationExtent().setSiteName("My site");
//		JAXBContext jaxbContext = JAXBContext.newInstance(RBVMetadataExtent.class, RBVMetadata.class);
//		Marshaller marshaller = jaxbContext.createMarshaller();
//		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//		RBVMetadata metadata = new RBVMetadata();
//		metadata.setRbvExtent(extent);
//		metadata.setUuid("1234");
//		metadata.setResourceTitle("toto");
//		marshaller.marshal(extent, System.out);
//
//		String xml = MetadataTools.toISO19139(metadata);
//		System.out.println(xml);
//	}
	
	public static SedooMetadata getMultilingualMetadata()
	{
		SedooMetadata aux = new SedooMetadata();
		aux.setUuid("123456");
		String metadataLanguage = Iso6392LanguageConverter.FRENCH;
		aux.setMetadataLanguage(metadataLanguage);
		
		HashMap<String, String> titles = new HashMap<String, String>();
		titles.put(Iso6392LanguageConverter.FRENCH, "titre francais");
		titles.put(Iso6392LanguageConverter.ENGLISH, "English title");
		
		aux.setResourceTitle(MetadataTools.toLocaleMap(titles), MetadataTools.getDefaultValue(titles, metadataLanguage));
		return aux;
	}

}
