package fr.sedoo.commons.metadata.utils;


import junit.framework.Assert;

import org.junit.Test;

import fr.sedoo.commons.metadata.shared.constant.Iso19139Constants;
import fr.sedoo.commons.metadata.utils.domain.MetadataTools;
import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.commons.metadata.utils.domain.patch.temporalextend.TemporalExtendPatch;
import fr.sedoo.commons.metadata.utils.domain.patch.temporalextend.TimePeriod;
import fr.sedoo.commons.test.TestTools;

public class TestSpecificEntries 
{
	
//	@Test
//	public void testDateFormatter() throws Exception 
//	{
//		String stringDate="2012-09-25";
//		Assert.assertEquals("La date doit être reformatée à l'identique",stringDate, MetadataTools.formatDate(MetadataTools.parseStringToDate(stringDate)));
//	}
//
	@Test
	public void testPbIntervalleTemporel() throws Exception
	{
		String xml = TestTools.getResourceFileContent(this.getClass(), "34e13658-9717-4f47-ab1c-cbfbb05f56f9.txt");
		SedooMetadata metadata = MetadataTools.fromISO19139(xml);
		Assert.assertEquals(metadata.getResourceBeginDate(), "2007-01-01");
		Assert.assertEquals(metadata.getResourceEndDate().toLowerCase(), Iso19139Constants.NOW.toLowerCase());
		xml = TestTools.getResourceFileContent(this.getClass(), "34e13658-9717-4f47-ab1c-cbfbb05f56f9-2.txt");
		metadata = MetadataTools.fromISO19139(xml);
		Assert.assertEquals(metadata.getResourceBeginDate(), "2007-01-01");
		Assert.assertEquals(metadata.getResourceEndDate().toLowerCase(),"2008-12-12");
	}
	
	@Test
	public void testLocaleFR() throws Exception
	{
		String xml = TestTools.getResourceFileContent(this.getClass(), "de5e7c54-2b13-445b-a2dc-b8145b846a98.txt");
		SedooMetadata metadata = MetadataTools.fromISO19139(xml);
		Assert.assertNotNull(metadata);
	}
	
	@Test
	public void testBasicReplace() throws Exception
	{
		String src ="<gmd:LocalisedCharacterString locale=\"#FR\">";
		String tgt ="<gmd:LocalisedCharacterString locale=\"#locale-fra\">";
		String res = MetadataTools.correctPreUnmarshallingProblems(src);
		Assert.assertEquals(tgt,res);
		
		String xml = TestTools.getResourceFileContent(this.getClass(), "de5e7c54-2b13-445b-a2dc-b8145b846a98.txt");
		res = MetadataTools.correctPreUnmarshallingProblems(xml);
	}
		
}
