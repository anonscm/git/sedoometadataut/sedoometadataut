package fr.sedoo.commons.metadata.shared.constant;
public interface Iso19139Constants {

	public final String NOW = "now";
	public final String DATASET = "dataset";
	public final String SERIES = "series";

}