package fr.sedoo.commons.metadata.utils.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WrappedString {
	
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static List<String> toStringList(List<WrappedString> codes) 
	{
		List<String> result = new ArrayList<String>();
		if (codes != null)
		{
			Iterator<WrappedString> iterator = codes.iterator();
			while (iterator.hasNext()) 
			{
				WrappedString wrappedString = (WrappedString) iterator.next();
				result.add(wrappedString.getValue());
			}
		}
		return result;
	}

}
