package fr.sedoo.commons.metadata.shared;

import java.io.Serializable;

public interface ResourceLink extends Serializable {
	
	public final static String LINK_PROTOCOL = "WWW:LINK-1.0-http--link";
	public final static String DOWNLOAD_PROTOCOL = "WWW:DOWNLOAD-1.0-http--download";
	public final static String WMS_SERVICE = "OGC:WMS-1.1.1-http-get-map";
	public final static String WFS_SERVICE = "OGC:WFS-1.0.0-http-get-capabilities";
	public static final String DEFAULT_PROTOCOL = "default";
	
	String getLink();
	String getLabel();
	String getProtocol();

}
