package fr.sedoo.commons.metadata.shared;

public class DefaultResourceLink implements ResourceLink
{
	private String link;
	private String label;
	private String protocol;
	
	public DefaultResourceLink()
	{
		
	}
	
	public DefaultResourceLink(ResourceLink resourceLink)
	{
		setLink(resourceLink.getLink());
		setLabel(resourceLink.getLabel());
		setProtocol(resourceLink.getProtocol());
	}

	@Override
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	
	
}
