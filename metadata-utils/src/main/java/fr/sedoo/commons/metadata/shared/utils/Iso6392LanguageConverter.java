package fr.sedoo.commons.metadata.shared.utils;

import fr.sedoo.commons.util.LocaleConstants;


public class Iso6392LanguageConverter {

	public static final String ENGLISH = "eng";
	public static final String FRENCH = "fre";
	public static final String PORTUGUESE = "por";
	public static final String SPANISH = "spa";

	public static String convertLocaleToIso6392(String language) {
		if (language.compareToIgnoreCase(LocaleConstants.ENGLISH) == 0) {
			return ENGLISH;
		}
		if (language.compareToIgnoreCase(LocaleConstants.FRENCH) == 0) {
			return FRENCH;
		} else {
			return language;
		}
	}

	public static String convertIso6392Tolocale(String language) {
		if (language.compareToIgnoreCase(ENGLISH) == 0) {
			return LocaleConstants.ENGLISH;
		}
		if (language.compareToIgnoreCase(FRENCH) == 0) {
			return LocaleConstants.FRENCH;
		} else {
			return language;
		}
	}

}