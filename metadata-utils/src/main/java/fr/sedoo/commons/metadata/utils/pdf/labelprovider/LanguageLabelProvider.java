package fr.sedoo.commons.metadata.utils.pdf.labelprovider;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class LanguageLabelProvider {

	private static Map<String, Locale> localeMap;


	public static String getLanguageFromCode(String code, Locale locale)
	{
		if (StringUtils.isEmpty(code))
		{
			return "";
		}

		else
		{
			if (localeMap == null)
			{
				init();
			}

			Locale aux = localeMap.get(code);
			if (aux == null)
			{
				return code;
			}
			else
			{
				return StringUtils.capitalize(aux.getDisplayLanguage(locale));
			}
		}
	}



	private static void init() 
	{
		String[] languages = Locale.getISOLanguages();
		localeMap = new HashMap<String, Locale>();
		for (String language : languages) {
			Locale locale = new Locale(language);
			localeMap.put(locale.getISO3Language(), locale);
		}
		//On ajoute le cas "fre"
		localeMap.put("fre", Locale.FRENCH);
	}

}
