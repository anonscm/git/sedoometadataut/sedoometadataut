package fr.sedoo.commons.metadata.utils.tools;

import java.io.ByteArrayInputStream;

import org.apache.commons.digester3.Digester;

import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.commons.metadata.utils.domain.MetadataTools;

public class MetadataUnmarshaller 
{
	 
	

	public static SedooMetadata fromXML(String xml)
	{
		
		Digester digester = new Digester();
        digester.setValidating(false);
        digester.addObjectCreate("gmd:MD_Metadata",SedooMetadata.class);
        digester.addBeanPropertySetter("*/gmd:abstract/gco:CharacterString","resourceAbstract");
        digester.addBeanPropertySetter("*/gmd:title/gco:CharacterString","resourceTitle");
        
        try {
        	SedooMetadata result = digester.parse(new ByteArrayInputStream(xml.getBytes()));
			return result;
		} catch (Exception e) 
		{
			return MetadataTools.getEmptyMetadata();
		}
	}
	
}
