package fr.sedoo.commons.metadata.utils.domain.patch.temporalextend;

import java.io.ByteArrayInputStream;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.DigesterLoader;

public class TemporalExtendPatch {
	
	public static TimePeriod getFirstTimePeriodFromXml(String xml) throws Exception
	{
		DigesterLoader digesterLoader = DigesterLoader.newLoader(new TimePeriodModule());
		Digester digester = digesterLoader.newDigester();
        digester.setValidating(false);
        TimePeriodList result = digester.parse(new ByteArrayInputStream(xml.getBytes()));
        if ((result == null) || (result.size()==0))
        {
        	return new TimePeriod();
        }
        else
        {
        	return result.get(0);
        }
	}

}
