package fr.sedoo.commons.metadata.utils.pdf.labelprovider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;

import fr.sedoo.commons.metadata.utils.LabelProvider;

public class RoleLabelProvider extends PropertyLabelProvider{

	public static final String AUTHOR = "author";
	public static final String ORIGINATOR = "originator";
	public static final String DISTRIBUTOR = "distributor";
	public static final String OWNER = "owner";
	public static final String USER = "user";
	public static final String POINT_OF_CONTACT = "pointOfContact";
	public static final String RESOURCE_PROVIDER = "resourceProvider";
	public static final String CUSTODIAN = "custodian";
	public static final String PROCESSOR = "processor";
	public static final String PRINCIPAL_INVESTIGATOR = "principalInvestigator";
	public static final String PUBLISHER = "publisher";
	
	private HashMap<String, String> labels = null; 
	
	public String getLabel(String key) 
	{
		if (labels == null)
		{
			initLabels(null);
		}
		String aux = labels.get(key.toLowerCase().replace("_", ""));
		if (aux == null)
		{
			aux ="";
		}
		return aux;
	}
	
	public RoleLabelProvider(Locale locale) 
	{
		initLabels(locale);
	}
	
	private void initLabels(Locale locale) 
	{
		labels = new LinkedHashMap<String, String>();
		LabelProvider labelProvider = new LabelProvider(locale);
		String aux = labelProvider.getLabel("person.roleItems");
		initLabels(aux, labels);
	}


}
