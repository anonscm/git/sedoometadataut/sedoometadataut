package fr.sedoo.commons.metadata.utils.domain;

public class I18nDescribedURL extends DescribedURL{
	
	private String link;
	private String frenchLabel;
	private String englishLabel;
	
	/**
	 * Used by unmarshalling
	 */
	public I18nDescribedURL()
	{
		
	}
	
	public I18nDescribedURL(String link, String frenchLabel, String englishLabel) {
		this.setLink(link);
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	
	
	public String getLabel()
	{
		throw new RuntimeException("Illegal call of getLabel for I18nDescribedURL");
	}

}
