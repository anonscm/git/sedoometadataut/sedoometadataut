package fr.sedoo.commons.metadata.utils.domain;

import org.apache.commons.lang.StringUtils;


public class Contact {

	private String individualName="";
	private String organisationName="";
	private String emailAddress="";
	private String role="";
	private String address;
	private String zipCode;
	private String city;
	private String country;
	
	public String getIndividualName() {
		return individualName;
	}
	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}
	public String getOrganisationName() {
		return organisationName;
	}
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getCompleteAddress()
	{
		StringBuffer sb = new StringBuffer();
		if (StringUtils.isEmpty(address)== false)
		{
			sb.append(address.trim()+"|");
		}
		if (StringUtils.isEmpty(zipCode)== false)
		{
			sb.append(zipCode.trim()+" ");
		}
		if (StringUtils.isEmpty(city)==false)
		{
			sb.append(city.trim()+"|");
		}
		if (StringUtils.isEmpty(country)==false)
		{
			sb.append(country);
		}
		return sb.toString().trim();
	}

}


