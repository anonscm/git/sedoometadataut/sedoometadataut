package fr.sedoo.commons.metadata.utils.pdf.labelprovider;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import fr.sedoo.commons.metadata.utils.LabelProvider;


public class StatusLabelProvider extends PropertyLabelProvider {

	public final static String PLANNED="planned";
	public final static String ON_GOING="onGoing";
	public final static String COMPLETED="completed";
	public final static String HISTORICAL_ARCHIVE="historicalArchive";
	public final static String OBSOLETE="obsolete";
	public final static String REQUIRED="required";
	public final static String UNDER_DEVELOPMENT="underDevelopment";
	
	private Map<String, String> labels = null; 

	public StatusLabelProvider(Locale locale) 
	{
		initLabels(locale);
	}

	public String getLabel(String key) 
	{
		if ((key == null) || (key.trim().length() == 0))
		{
			return "";
		}
		if (labels == null)
		{
			initLabels(null);
		}
		String label = labels.get(key.toLowerCase().replace("_", ""));
		if (label != null)
		{
			return label;
		}
		else
		{
			return "";
		}
	}

	private void initLabels(Locale locale) 
	{
		labels = new LinkedHashMap<String, String>();
		LabelProvider labelProvider = new LabelProvider(locale);
		String aux = labelProvider.getLabel("metadataEditing.statusItem");
		initLabels(aux, labels);
	}

}
