package fr.sedoo.commons.metadata.utils.pdf.labelprovider;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import fr.sedoo.commons.metadata.utils.LabelProvider;

public class UpdateRythmLabelProvider extends PropertyLabelProvider {

	public final static String CONTINUAL="continual";
	public final static String DAILY="daily";
	public final static String WEEKLY="weekly";
	public final static String FORTNIGHTLY="fortnightly";
	public final static String MONTHLY="monthly";
	public final static String QUARTERLY="quarterly";
	public final static String BIANNUALLY="biannually";
	public final static String ANNUALLY="annually";
	public final static String AS_NEEDED="asNeeded";
	public final static String IRREGULAR="irregular";
	public final static String NOT_PLANNED="notPlanned";
	public final static String UNKNOWN="unknown";
	
	private Map<String, String> labels = null;
	
	/**
	 * @param key
	 * @return Le libellé internationalisé si la clé est trouvée dans la liste - unknown sinon.
	 */
	public String getLabel(String key) 
	{
		String aux = key;
		if (labels == null)
		{
			initLabels(null);
		}
		if ((key == null) || (key.trim().length() == 0))
		{
			return "";
		}
		String label = labels.get(aux.toLowerCase().replace("_", ""));
		if (label != null)
		{
			return label;
		}
		else
		{
			return "";
		}
	}

	private void initLabels(Locale locale) 
	{
		labels = new LinkedHashMap<String, String>();
		LabelProvider labelProvider = new LabelProvider(locale);
		String aux = labelProvider.getLabel("metadataEditing.updateRythmItem");
		initLabels(aux, labels);
	}
	

	public Map<String, String> getLabels()
	{
		if (labels == null)
		{
			initLabels(null);
		}
		return labels;
	}
	
	public UpdateRythmLabelProvider(Locale locale) 
	{
		initLabels(locale);
	}


}
