package fr.sedoo.commons.metadata.utils.xml;

import org.geotoolkit.lang.Static;


public final class RBVNamespaces extends Static {
	
	 private RBVNamespaces() {
	    }

		 
        public static final String GMD_NAMESPACE = "http://www.isotc211.org/2005/gmd";
	    
	    public static final String GMD_PREFIX = "gmd";
		 
	    public static final String RBV_NAMESPACE = "http://www.sedoo.fr/rbv";
	    
	    public static final String RBV_PREFIX = "rbv";

}
