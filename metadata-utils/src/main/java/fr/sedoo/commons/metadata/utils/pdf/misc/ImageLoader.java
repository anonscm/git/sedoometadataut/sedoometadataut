package fr.sedoo.commons.metadata.utils.pdf.misc;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import fr.sedoo.commons.metadata.utils.pdf.PrintService;

public class ImageLoader {

	
	public static File loadImageFromUrl(String urlString, String ext) throws IOException
	{
		URL url = new URL(urlString);
		URLConnection urlConnection = url.openConnection();
		BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
		// my local file writer, output stream
		File tmpFile = File.createTempFile(PrintService.RBV_PREFIX, ext);
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(tmpFile));
		int i;
		while ((i = in.read()) != -1) {
		    out.write(i);
		}
		out.flush();
		out.close();
		in.close();
		return tmpFile;
	}
}
