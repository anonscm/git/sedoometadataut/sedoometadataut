package fr.sedoo.commons.metadata.utils;

import java.util.Locale;
import java.util.ResourceBundle;

public class LabelProvider {

	Locale locale;
	ResourceBundle bundle;
	
	public LabelProvider()
	{
		this(null);
	}
	
	public LabelProvider(final Locale locale)
	{
		if (locale == null)
		{
			this.locale = Locale.ENGLISH;
		}
		else
		{
			this.locale = locale; 
		}
	}
	
	public String getLabel(String key)
	{
		if (bundle == null)
		{
			bundle = ResourceBundle.getBundle("fr.sedoo.commons.metadata.utils.Message", locale, new UTF8Control());
		}
		return bundle.getString(key).replace("''", "'");
	}
	
}
