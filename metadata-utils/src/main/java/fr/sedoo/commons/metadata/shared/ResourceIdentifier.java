package fr.sedoo.commons.metadata.shared;

import java.io.Serializable;

public interface ResourceIdentifier extends Serializable {
	String getCode();
	void setCode(String code);
	String getNameSpace();
	void setNameSpace(String nameSpace);
}
