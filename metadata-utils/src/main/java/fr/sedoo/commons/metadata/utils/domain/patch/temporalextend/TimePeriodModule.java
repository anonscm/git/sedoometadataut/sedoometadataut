package fr.sedoo.commons.metadata.utils.domain.patch.temporalextend;

import org.apache.commons.digester3.binder.AbstractRulesModule;

public class TimePeriodModule extends AbstractRulesModule 
{

	@Override
	public void configure()
	{
		forPattern("gmd:MD_Metadata").createObject().ofType(TimePeriodList.class).then().setProperties(); 
		forPattern("*/gml:TimePeriod").createObject().ofType(TimePeriod.class).then().setProperties().then().setNext("add");
		forPattern("*/gml:TimePeriod/gml:beginPosition").setBeanProperty().withName("beginPosition");
		forPattern("*/gml:TimePeriod/gml:endPosition").setProperties().then().setBeanProperty().withName("endPosition");
	}

}
