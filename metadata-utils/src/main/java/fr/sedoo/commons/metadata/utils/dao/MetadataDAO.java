package fr.sedoo.commons.metadata.utils.dao;

import java.util.List;

import org.opengis.metadata.Metadata;

import fr.sedoo.commons.domain.User;
import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.commons.metadata.utils.domain.Summary;


public interface MetadataDAO
{
	static final String METADATA_DAO_BEAN_NAME = "metadataDAO";
	Metadata getMetadataById(String id, User user) throws Exception;
	List<Summary> getSummaries(User user) throws Exception;
	String getPDFURL(String metadataId, User user) throws Exception;
	boolean deleteMetadataByUuid(String uuid, User user) throws Exception;
	SedooMetadata saveMetadata(SedooMetadata metadata, User user) throws Exception;
}
