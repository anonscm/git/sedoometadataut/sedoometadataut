package fr.sedoo.commons.metadata.shared;


public class DefaultResourceIdentifier implements ResourceIdentifier {
	
	private String code;
	private String nameSpace;
	
	public DefaultResourceIdentifier()
	{
	}
	
	public DefaultResourceIdentifier(String code, String nameSpace) {
		this.code = code;
		this.nameSpace = nameSpace;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getNameSpace() {
		return nameSpace;
	}
	public void setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
	}

}
