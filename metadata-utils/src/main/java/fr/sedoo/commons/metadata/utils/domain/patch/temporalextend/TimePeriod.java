package fr.sedoo.commons.metadata.utils.domain.patch.temporalextend;

public class TimePeriod {
	
	private String beginPosition;
	private String endPosition;
	private String indeterminatePosition;
	
	public String getBeginPosition() {
		return beginPosition;
	}
	public void setBeginPosition(String beginPosition) {
		this.beginPosition = beginPosition;
	}
	public String getEndPosition() {
		return endPosition;
	}
	public void setEndPosition(String endPosition) {
		this.endPosition = endPosition;
	}
	public String getIndeterminatePosition() {
		return indeterminatePosition;
	}
	public void setIndeterminatePosition(String indeterminatePosition) {
		this.indeterminatePosition = indeterminatePosition;
	}

}
